# 寒假项目

## git操作篇

### 1、项目克隆
首先所有成员对项目进行克隆，使用如下命令
git clone https://gitee.com/spruce0/winter-project.git
克隆之后进入项目，目录应该为这样子：

![](README_md_files/image.png?v=1&type=image)

### 2、创建自己的分支
其次，再次右键git bash，弹出git窗口
创建自己的分支进行代码管理，命令为 git checkout -b wei（创建名为wei的分支）
![git创建分支](README_md_files/git-01.png?v=1&type=image)

### 3、提交三步骤
* git add .
* git commit -m"提交项目的注释（说明）"
* git push origin wei（提交到远程的哪一个分支，自己就填自己的分支即可）

## vue篇

### 启动项目
1、 首先打开命令行在项目路径下cmd
![输入图片描述](README_md_files/vue-01.png?v=1&type=image)
2、输入 ==npm install== 进行依赖安装
3、输入 ==npm run serve== 启动项目![输入图片描述](README_md_files/vue-02.png?v=1&type=image)
