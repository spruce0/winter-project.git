import Vue from 'vue'
import Vuex from 'vuex'
import { Storage, Session } from '../assets/js/lib.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    allData: [], // 全部数据
    filterVal: '',
    topicList: JSON.parse(Storage.get('topicList') || '[]'),
    loginId: Session.get('loginId'),
    sectionAllData: [], // 包含评论的数据
    userSet: JSON.parse(Session.get('userSet')),
    users: ''
  },
  mutations: {
    initAllData (state, data) {
      state.allData = data
    },
    addTopic (state, topicObj) {
      // const topicList = JSON.parse(Storage.get('topicList'))
      state.topicList.push(topicObj)
      Storage.set('topicList', JSON.stringify(state.topicList))
    },
    // setTopicCount (state, newData) {
    //   const list = state.topicList.filter(t => {
    //     if (t.id) {
    //       if (t.id === newData.id) {
    //         t.visit_count = newData.visit_count
    //         return t
    //       }
    //     } else {
    //       return newData
    //     }
    //   })
    //   console.log(list)
    //   Storage.set('topicList', JSON.stringify(state.topicList))
    // },
    setFilterVal (state, newVal) {
      state.filterVal = newVal
    },
    initSectionAllData (state, data) {
      state.sectionAllData = data
    },
    setUser (state, userObj) {
      state.users = userObj
    }
  },
  actions: {
  },
  getters: {
  },
  modules: {
  }
})
